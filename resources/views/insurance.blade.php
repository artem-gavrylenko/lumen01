@extends('layout')

@section('content')
    <form action="/insurance" method="post">
        <fieldset>
            <legend>Insurance for Client</legend>
            @if ($errors)
                <div class="form-group">
                    <label for="output">Errors:</label>
                    <output name="result" id="output">
                        @foreach ($errors as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </output>
                </div>
            @endif
            <div class="form-group">
                <label for="client_id">Client</label>
                <select id="client_id" name="client_id" class="form-control">
                    @if($clients)
                        @foreach($clients as $client)
                            <option value="{{ $client->id  }}">{{ $client->name  }} {{ $client->surname  }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="insurance_type_id">Insurance type</label>
                <select id="insurance_type_id" name="insurance_type_id" class="form-control">
                    @if($insurance_types)
                        @foreach($insurance_types as $insurance_type)
                            <option value="{{ $insurance_type->id  }}">{{ $insurance_type->name  }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="amount">Amount</label>
                <input type="number" id="amount" name="amount" class="form-control">
            </div>
            <div class="form-group">
                <label for="beginning_at">Beginning at</label>
                <input type="datetime-local" id="beginning_at" name="beginning_at" class="form-control" value="{{ $current_time  }}">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </fieldset>
    </form>
@stop