@extends('layout')

@section('content')
    <form action="/insurance_type" method="post">
        <fieldset>
            <legend>Insurance</legend>
            @if ($errors)
                <div class="form-group">
                    <label for="output">Errors:</label>
                    <output name="result" id="output">
                        @foreach ($errors as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </output>
                </div>
            @endif
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="term">Term</label>
                <input type="datetime-local" id="term" name="term" class="form-control">
            </div>
            <div class="form-group">
                <label for="percent">Percent</label>
                <input type="number" id="percent" name="percent" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </fieldset>
    </form>
@stop