@extends('layout')

@section('content')
    <form action="/register" method="post">
        @if ($errors)
            <div class="form-group">
                <label for="output">Errors:</label>
                <output name="result" id="output">
                    @foreach ($errors as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </output>
            </div>
        @endif
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" id="name" name="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="surname">Surname</label>
            <input type="text" id="surname" name="surname" class="form-control">
        </div>
        <div class="form-group">
            <label for="surname">Passport</label>
            <input type="text" id="passport" name="passport" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@stop