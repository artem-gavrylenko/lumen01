@extends('layout')

@section('content')
    <div class="row">
        <div class="col-sm">
            <div class="card">
                <div class="card-header">
                    Top Clients
                </div>
                <ul class="list-group list-group-flush">
                    @foreach($clients as $client)
                        <li class="list-group-item">{{ $client->name }} {{ $client->surname }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-sm">
            <div class="card">
                <div class="card-header">
                    Popular Insurance
                </div>
                <ul class="list-group list-group-flush">
                    @foreach($insurance_types as $insurance_type)
                        <li class="list-group-item">{{ $insurance_type->name }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@stop