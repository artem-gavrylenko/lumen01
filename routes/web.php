<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', [
    'as' => 'main',
    'uses' => 'IndexController@index'
]);

$router->get('/register', [
    'as' => 'register',
    'uses' => 'ClientController@index'
]);

$router->post('/register', 'ClientController@register');

$router->get('/insurance', [
    'as' => 'insurance',
    'uses' => 'InsuranceController@index'
]);

$router->post('/insurance', 'InsuranceController@insurance');

$router->get('/insurance_type', [
    'as' => 'insurance_type',
    'uses' => 'InsuranceTypeController@index'
]);

$router->post('/insurance_type', 'InsuranceTypeController@insuranceType');