<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

class ClientController extends Controller
{
    protected $view = 'register';

    public function index()
    {
        return $this->view($this->view);
    }

    public function register(Request $request)
    {
        $name = $request->get('name');
        $surname = $request->get('surname');
        $passport = $request->get('passport');

        $client = new Client;
        $validation = $client->validate($request);

        if($validation->fails()) {
            return $this->view($this->view, [
                'errors' => $validation->errors()->all()
            ]);
        }

        $client = Client::where('name', $name)
                ->where('surname', $surname)
                ->first() ?? new Client;

        $client->name = $name;
        $client->surname = $surname;
        $client->passport = $passport;
        $client->save();

        return redirect('/');
    }

    private function view($layout, $mergeData = [])
    {
        return view($layout, array_merge([
            'errors' => []
        ], $mergeData));
    }
}
