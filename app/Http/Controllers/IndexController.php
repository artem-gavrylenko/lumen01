<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Insurance;
use App\InsuranceType;

class IndexController extends Controller
{
    public function index()
    {
        $clients = Client::all();
        $insurance = Insurance::all();
        $insurance_types = InsuranceType::all();

        return view('index', [
            'clients' => $clients,
            'insurance' => $insurance,
            'insurance_types' => $insurance_types
        ]);
    }
}
