<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Insurance;
use App\InsuranceType;

class InsuranceController extends Controller
{
    protected $view = 'insurance';

    public function index()
    {
        return $this->view($this->view);
    }

    public function insurance(Request $request)
    {
        $client_id = $request->get('client_id');
        $insurance_type_id = $request->get('insurance_type_id');
        $amount = $request->get('amount');
        $beginning_at = $request->get('beginning_at');

        $insurance = new Insurance;
        $validation = $insurance->validate($request);

        if($validation->fails()) {
            return $this->view($this->view, [
                'errors' => $validation->errors()->all()
            ]);
        }

        $insurance = Insurance::where('client_id', $client_id)
                ->where('insurance_type_id', $insurance_type_id)
                ->first() ?? new Insurance;

        $insurance->client_id = $client_id;
        $insurance->insurance_type_id = $insurance_type_id;
        $insurance->amount = $amount;
        $insurance->beginning_at = $beginning_at;
        $insurance->save();

        return redirect('/');
    }

    private function view($layout, $mergeData = [])
    {
        $clients = Client::all();
        $insuranceType = InsuranceType::all();

        return view($layout, array_merge([
            'errors' => [],
            'clients' => $clients,
            'insurance_types' => $insuranceType,
            'current_time' => date('Y-m-d\TH:i')
        ], $mergeData));
    }
}
