<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InsuranceType;

class InsuranceTypeController extends Controller
{
    protected $view = 'insurance_type';

    public function index()
    {
        return view($this->view);
    }

    public function insuranceType(Request $request)
    {
        $name = $request->get('name');
        $term = $request->get('term');
        $percent = $request->get('percent');

        $insurance_type = new InsuranceType;
        $validation = $insurance_type->validate($request);

        if($validation->fails()) {
            return view($this->view, [
                'errors' => $validation->errors()->all()
            ]);
        }

        $insurance_type = InsuranceType::where('name', $name)
                ->first() ?? new InsuranceType;

        $insurance_type->name = $name;
        $insurance_type->term = $term;
        $insurance_type->percent = $percent;
        $insurance_type->save();

        return redirect('/');
    }
}
