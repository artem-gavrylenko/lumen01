<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InsuranceType extends Model
{
    protected $table = 'insurance_types';

    private $rules = [
        'name' => 'required|unique:insurance_types',
        'term' => 'required',
        'percent' => 'required'
    ];

    public function validate(Request $request)
    {
        return Validator::make($request->all(), $this->rules);
    }
}