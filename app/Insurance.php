<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Insurance extends Model
{
    protected $table = 'insurance';

    private $rules = [
        'client_id' => 'required',
        'insurance_type_id' => 'required',
        'amount' => 'required|numeric',
        'beginning_at' => 'required'
    ];

    public function validate(Request $request)
    {
        return Validator::make($request->all(), $this->rules);
    }
}