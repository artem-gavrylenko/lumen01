<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class Client extends Model
{
    protected $table = 'clients';

    private $rules = [
        'name' => 'required',
        'surname' => 'required',
        'passport' => 'required|unique:clients'
    ];

    public function validate(Request $request)
    {
        return Validator::make($request->all(), $this->rules);
    }
}